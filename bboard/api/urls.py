from django.urls import path

from .views import bbs_list, BbDetailView, comments


urlpatterns = [
    path('bbs/<int:pk>/comments/', comments),
    path('bbs/<int:pk>/', BbDetailView),
    path('bbs/', bbs_list),
]
